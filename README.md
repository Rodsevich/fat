# FAT (Flutter Assisting Tool)
A tool made to facilitate your everyday development by providing your projects with scaffolding boilerplate code generations that both speed up the development cycle and stablish a consistent and extensible way of coding your apps.

## Features
- [ ] _Mustachex templates scaffolding_
- [ ] _Interactive code generation scripts_
- [ ] Analysis Server/IDE integration
- [ ] Global configurations
- [ ] Backward generations modifications
- [x] done
- [ ] To Do
- [ ] _doing_